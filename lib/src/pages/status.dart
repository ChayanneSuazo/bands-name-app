import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:band_names_app/src/providers/socket_provider.dart';

class StatusPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final socketProvider = Provider.of<SocketProvider>(context);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Server Status: ${socketProvider.serverStatus}'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.message),
        onPressed: () => buttonAction(socketProvider),
      ),
    );
  }

  void buttonAction(SocketProvider socketProvider) {
    socketProvider.socket.emit('broadcast-message',
        {'name': "Flutter", "message": "Hello from Flutter"});
  }
}
